# SPDX-License-Identifier: GPL-3.0
#
# A GDB plugin to attach to processes without knowing their PIDs
#
# Copyright © 2018 Red Hat
# Authors:
#   Lyude Paul <lyude@redhat.com>

from __future__ import print_function
from builtins import super

from sys import stderr
from datetime import datetime
from fnmatch import fnmatch, fnmatchcase
from textwrap import dedent, wrap
import platform

try:
    import psutil
except ModuleNotFoundError:
    raise gdb.GdbError(("pattach: This script requires psutil, but I couldn't "
                        "find it!"))

if platform.system() == 'Linux':
    for p in psutil.process_iter(['ppid', 'name']):
        if p.info['ppid'] == 0 and p.info['name'] == 'kthreadd':
            kthreadd = p.pid

    if kthreadd is None:
        raise Exception("Could not find kthreadd?")

    def process_iter(attrs=None, ad_value=None):
        if attrs is None:
            attrs = ['ppid']
        else:
            attrs += ['ppid']

        for process in psutil.process_iter(attrs, ad_value):
            # Filter out kernel threads, since we'll never be able to actually
            # attach to them
            if process.info['ppid'] != kthreadd and process.pid != kthreadd:
                yield process
else:
    from psutil import process_iter

class PAttachSearchKind(gdb.Command):
    """
    If multiple processes are found in the search, pattach will ask you which
    one to attach to.
    """
    def __init__(self, parent):
        self.__doc__ = dedent(self.__doc__ + PAttachSearchKind.__doc__).strip()

        super().__init__("pattach %s" % self.NAME, gdb.COMMAND_NONE)
        self.parent = parent

    def check_match(self, process, arg):
        raise NotImplementedError()

    def parse_args(self, args):
        # We do nothing by default
        return args

    def choose_process_prompt(search_results):
        print("%d processes found:" % len(search_results))
        for i, result in enumerate(search_results):
            with result.oneshot():
                creation_time = datetime.fromtimestamp(result.create_time())

                print("  (%d) %d [%s] owned by %s created on %s: %s" % (
                    i + 1, result.pid, result.name(), result.username(),
                    creation_time.astimezone().strftime('%x %X'),
                    result.cmdline()))

        choice = None
        while choice is None:
            try:
                in_str = input("Which process should I use? ")
                if in_str == "":
                    return

                choice = search_results[int(in_str) - 1]
            except KeyboardInterrupt:
                return
            except EOFError:
                print('')
                return
            except (ValueError, IndexError):
                print("Invalid parameter '%s'" % in_str, file=stderr)

        return choice

    def invoke(self, arg, from_tty):
        self.dont_repeat()
        search_results = []
        access_was_denied = False

        if arg == "":
            raise gdb.GdbError("No search arguments specified")

        arg = self.parse_args(arg)
        inferiors = set(i.pid for i in gdb.inferiors() if i.is_valid())

        for process in process_iter():
            try:
                with process.oneshot():
                    if not self.check_match(process, arg):
                        continue

                    if process.pid in inferiors:
                        print("Skipping PID %d [%s] as it's already attached" %
                              (process.pid, process.name()))
                    else:
                        search_results.append(process)
            except psutil.AccessDenied:
                access_was_denied = True

        if access_was_denied:
            print('(some processes could not be checked due to permission errors)')

        count = len(search_results)
        if count == 0:
            raise gdb.GdbError("No matches for '%s'" % arg)
        elif count == 1:
            result = search_results[0]
            with result.oneshot():
                print('Found PID %d [%s] owned by %s with cmdline %s' % (
                    result.pid, result.name(), result.username(), result.cmdline()))
        else:
            result = PAttachSearchKind.choose_process_prompt(search_results)
            if result is None:
                return

        self.parent.saved_search = (self, arg)

        try:
            gdb.execute("attach %d" % result.pid, from_tty)
        except gdb.error as e:
            raise gdb.GdbError("Failed to attach to %d: %s" %
                               (result.pid, e.args[0])) from e

class PAttachNameSearch(PAttachSearchKind):
    """
    Search for and attach to a process with a name matching the given string

    POSIX style shell globs are also understood.
    """
    NAME = 'name'

    def parse_args(self, args):
        return gdb.string_to_argv(args)[0]

    def check_match(self, process, arg):
        return fnmatchcase(process.name(), arg)

    def complete(self, text, word):
        text_in_quotes = text.startswith("'")
        if text_in_quotes:
            # Strip the beginning quote for the search
            text = text[1:]
        elif not text.isalnum() and ' ' in word:
            # I have literally no fucking idea why this works, but somehow
            # this indicates to GDB that the completion item should be in
            # quotes
            return gdb.COMPLETE_NONE

        results = []
        for p in process_iter(['name']):
            if not p.info['name'].startswith(text):
                continue

            result = p.info['name']
            if text_in_quotes or not result.isalnum():
                result = "'%s'" % result

            results.append(result)

        return results

class PAttachCmdlineSearch(PAttachSearchKind):
    """
    Search for and attach to a process containing the given string in its
    command line
    """
    NAME = 'cmdline'

    def check_match(self, process, arg):
        return arg in ' '.join(process.cmdline())

class PAttachExecutableSearch(PAttachSearchKind):
    """
    Search for and attach to a process with the given executable path

    POSIX style shell globs are also understood.
    """
    NAME = 'executable'

    def check_match(self, process, arg):
        return fnmatch(process.exe(), arg)

    def complete(self, text, word):
        return gdb.COMPLETE_FILENAME

class PAttach(gdb.Command):
    """ Redoes the last pattach search and attach, if any """
    def __init__(self):
        self.__doc__ = wrap(dedent(self.__doc__).strip(), width=78)
        super().__init__("pattach", gdb.COMMAND_RUNNING,
                         gdb.COMPLETE_NONE, True)
        self.saved_search = None

    def invoke(self, arg, from_tty):
        self.dont_repeat()

        if arg == "":
            if self.saved_search is None:
                raise gdb.GdbError("No previous search")

            kind, arg = self.saved_search
            print('Reusing previous search: %s "%s"' % (kind.NAME, arg))
            kind.invoke(arg, from_tty)
        else:
            raise gdb.GdbError("Invalid arguments '%s'" % arg)

parent = PAttach()

PAttachCmdlineSearch(parent)
PAttachExecutableSearch(parent)
PAttachNameSearch(parent)
