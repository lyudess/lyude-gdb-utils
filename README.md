# What is this?

It's a set of GDB plugins that I maintain for myself. They are /supposed/ to work with python2, but I don't check. So let me know if they don't!

# Required dependencies

- [https://pypi.org/project/psutil/](psutil)
