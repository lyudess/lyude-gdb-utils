# SPDX-License-Identifier: GPL-3.0
#
# A GDB plugin for printing the environment of inferiors
#
# Copyright © 2018 Red Hat
# Authors:
#   Lyude Paul <lyude@redhat.com>

from __future__ import print_function
from builtins import super

try:
    import psutil
except ModuleNotFoundError:
    raise gdb.GdbError(("pattach: This script requires psutil, but I"
                        " couldn't find it!"))

class ShowInferiorEnvironment(gdb.Command):
    def __init__(self):
        self.__doc__ = (
            "Print the environment variables of an inferior\n"
            "Usage: show inferior-environment [inferior_id1 [inferior_id2 ...]|all]\n"
            "\n"
            "If inferior_id is omitted, the environment of the currently"
            " selected inferior will be shown."
        )
        super().__init__("show inferior-environment", gdb.COMMAND_STATUS)

    def invoke(self, arg, from_tty):
        self.dont_repeat()

        if arg == "":
            # Current inferior
            inferiors = gdb.selected_inferior(),
        elif arg.lower() == "all":
            inferiors = gdb.inferiors()
        else:
            try:
                given_inferior_ids = set(int(a) for a in
                                         gdb.string_to_argv(arg))
            except ValueError as e:
                raise gdb.GdbError("Invalid arguments: %s" % e.args[0]) from e

            inferiors = gdb.inferiors()
            inferior_ids = set(i.num for i in inferiors)

            # Confirm all the IDs we were given actually exist
            for id_ in given_inferior_ids:
                if id_ not in inferior_ids:
                    raise gdb.GdbError("Unknown inferior number: %d" % id_)

            # Finally, filter out any inferiors we didn't select
            inferiors = [i for i in inferiors if i.num in given_inferior_ids]

        for i in inferiors:
            if not i.pid:
                raise gdb.GdbError("Selected inferior %d has no PID yet!" %
                                   i.num)

        # Finally, actually print
        multiple = len(inferiors) > 1
        indent = '  ' if multiple else ''
        for i in inferiors:
            process = psutil.Process(i.pid)
            if multiple:
                print('Inferior %d:' % i.num)
            for name, value in process.environ().items():
                print('%s%s=%s' % (indent, name, value))

    def complete(self, text, word):
        if text == "":
            return ["all"] + [str(i.num) for i in gdb.inferiors()]
        elif "all".startswith(text):
            return ["all"]
        elif text.isalnum():
            return [s for s in (str(i.num) for i in gdb.inferiors())
                    if s.startswith(text)]

        return None

ShowInferiorEnvironment()
